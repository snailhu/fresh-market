# freshMarket

#### 介绍
生鲜超市售卖系统，前端包含，登录，注册，购物车，下单，首页商品展示，商品详情，商品小常识，个人中心，我的订单
后端包含：首页，商品管理，上下架，热销产品推荐，设置，管理模块：分类管理，会员管理，订单管理，商品修改，这是，富文本编辑器。等

#### 软件架构

java语言开发  springboot + springmvc +mysql +maven


#### 使用说明

1.  客户端首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/1003/123227_ee3f2ea1_9817478.png "微信图片_20211003112916.png")
2.  商品展示
![输入图片说明](https://images.gitee.com/uploads/images/2021/1003/123330_59ad8bcf_9817478.png "新品.png")
3.  购物车
![输入图片说明](https://images.gitee.com/uploads/images/2021/1003/123354_3c5e2ad0_9817478.png "购物车.png")
4.后端登录首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/1003/123414_f2431157_9817478.png "管理端登录.png")
5.商品编辑
![输入图片说明](https://images.gitee.com/uploads/images/2021/1003/123512_124b2b02_9817478.png "商品编辑.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1003/123530_ca490f0e_9817478.png "商品编辑1.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5.如有任何疑惑的可+++微信。

![输入图片说明](9_1648966917.jpg)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
