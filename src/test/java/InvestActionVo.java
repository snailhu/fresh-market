import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

@Data
public class InvestActionVo extends BaseRowModel {

    /**
     * 第一列的数据
     */
    @ExcelProperty(index = 0)
    private String ieid;
    /**
     * 第二列的数据
     */
    @ExcelProperty(index = 1)
    private String instid;

    @ExcelProperty(index = 2)
    private String instnm;

    @ExcelProperty(index = 3)
    private String cmid;
    @ExcelProperty(index = 4)
    private String cmnm;

    @ExcelProperty(index = 5)
    private String cmabnm;
    @ExcelProperty(index = 6)
    private String invsum;
    @ExcelProperty(index = 7)
    private String i_currency;
    @ExcelProperty(index = 8)
    private String numsha;
    @ExcelProperty(index = 9)
    private String eqrt;
    @ExcelProperty(index = 10)
    private String ficsum;
    @ExcelProperty(index = 11)
    private String f_currency;
    @ExcelProperty(index = 12)
    private String t_eqrt;
    @ExcelProperty(index = 13)
    private String ficdt;
    @ExcelProperty(index = 14)
    private String fictp;
    @ExcelProperty(index = 15)
    private String ficrd;
    @ExcelProperty(index = 16)
    private String ficind;


}
