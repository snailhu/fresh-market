import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import it.mall.build.MallApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MallApplication.class)

@WebAppConfiguration
//@ContextConfiguration
public class ParseExcel {


    @Test
    public void parseExcel(){

//        try {
//            List<InvestActionVo> investActionVos = read("D:\\单单\\9.12数据\\数据-9.12\\表\\投资事件.xlsx", InvestActionVo.class);
////            List<InvestActionVo> investActionVos = read("D:/invest.xlsx", InvestActionVo.class);
//            System.out.println(investActionVos);
//            System.out.println("读取完成");
//        }catch (Exception e){
//            e.printStackTrace();
//        }


        simpleRead();
    }


    public <T extends BaseRowModel> List<T> read(String filename, Class<T> rowModel) throws Exception{

        ExcelListener excelListener = new ExcelListener();
        ExcelReader excelReader = getExcelReader(new File(filename),excelListener,true);
        if(excelReader == null){
            return new ArrayList();
        }
        for(Sheet sheet:excelReader.getSheets()){
            sheet.setClazz(rowModel);
            excelReader.read(sheet);
        }
        List<T> list = new ArrayList<>();
        for(Object obj:excelListener.getDataList()){
            list.add((T)obj);
        }
        return list;
    }

    /**
     *
     * @param file 文件
     * @param eventListener 用户监听器
     * @param b
     * @return
     */
    public static ExcelReader getExcelReader(File file, AnalysisEventListener eventListener, boolean b) throws Exception{
        String fileName  = file.getName();
        if (fileName == null ) {
            throw new Exception("文件格式错误！");
        }
        if (!fileName.toLowerCase().endsWith(ExcelTypeEnum.XLS.getValue()) && !fileName.toLowerCase().endsWith(ExcelTypeEnum.XLSX.getValue())) {
            throw new Exception("文件格式错误！");
        }
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(file);
            if (fileName.toLowerCase().endsWith(ExcelTypeEnum.XLS.getValue())) {
                return new ExcelReader(inputStream, ExcelTypeEnum.XLS, null, eventListener, false);
            } else {
                return new ExcelReader(inputStream, ExcelTypeEnum.XLSX, null, eventListener, false);
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public static void simpleRead() {

        // 读取 excel 表格的路径
        String readPath = "D:\\单单\\9.12数据\\数据-9.12\\表\\投资事件.xlsx";

        try {
            // sheetNo --> 读取哪一个 表单
            // headLineMun --> 从哪一行开始读取( 不包括定义的这一行，比如 headLineMun为2 ，那么取出来的数据是从 第三行的数据开始读取 )
            // clazz --> 将读取的数据，转化成对应的实体，需要 extends BaseRowModel
            Sheet sheet = new Sheet(1, 2, InvestActionVo.class);

            // 这里 取出来的是 ExcelModel实体 的集合
            List<Object> readList = EasyExcelFactory.read(new FileInputStream(readPath), sheet);
            // 存 ExcelMode 实体的 集合
            List<InvestActionVo> list = new ArrayList<InvestActionVo>();
            for (Object obj : readList) {
                list.add((InvestActionVo) obj);
            }

            // 取出数据
            System.out.println("over");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

}
